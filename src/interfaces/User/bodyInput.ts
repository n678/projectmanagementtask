interface existingPasswordBodyInputPartOne {
    existingPassword: string
}
interface existingPasswordBodyInputPartTwo {
    newPassword: string,
    confirmPassword: string
}

export {
    existingPasswordBodyInputPartOne,
    existingPasswordBodyInputPartTwo
}