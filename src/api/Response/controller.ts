import e, {
    Request,
    Response
} from "express"

//models
import User from "../../model/user"
import Mentor from "../../model/mentor"
import Assessment from "../../model/assesment"
import Student from "../../model/student"
import ResponseSchema from "../../model/response"


//utils file 
import slugGenerator from "../../../utils/slugGenerator"
import idGenerator from "../../../utils/idGenerator"
import paginationHandler from "../../../utils/paginationHandler"
import {
    queryWithFilterAndSearch,
    sortingOfAssessmentHandler
} from "../../../utils/controller/Assessment/query"
import {
    uploadAnyFile,
    uploadProfilePictureDefault
} from "../../../utils/fileOrPictureUploadHandler"

//interfaces
import AssessmentModelInterface from "../../../src/interfaces/Assesment/model"
import MentorModel from "../../../src/interfaces/Mentor/model"
import ResponseModel from "../../../src/interfaces/Response/model"
import {
    createResponseBodyInput,
    remarkAndGradingBodyInput
} from "../../../src/interfaces/Response/bodyInput"
import paginationReturnInterface from "../../interfaces/utils/paginationReturn"
import {
    createBodyInput,
    showAllAssessmentBodyInputForAll
} from "../../../src/interfaces/Assesment/bodyInput"
import MentorProfileInterface from "../../../src/interfaces/Mentor/model"
import {
    UpdateReturn
} from "../../interfaces/utils/mongoDB"
import {
    FileUploadDefault,
    FileUploadDefaultReturn
} from "../../interfaces/utils/fileUploadHandler"


//create a response for a assessment
const createNewResponseHandler: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const {
            file: {
                base64:uploadFileBase64,
                link: sharableFileLink
            },
            assessment_id
        }:createResponseBodyInput = req.body //get the body data
        //first check that is this student have already response or not 
        const studentProfile = await Student.findOne ({user:req.user._id})        
        const checkAvailability:null | ResponseModel = await ResponseSchema.findOne ( 
            {
                responsesBy: studentProfile?._id,
                assessment: assessment_id
            }
        )
        if (!checkAvailability) { //if student don't response yet
            //secondly check that is deadline is still available or not 
            const queryWithThatAssignment:null | AssessmentModelInterface = await Assessment.findOne (
                {
                    "isDelete": false,
                    _id: assessment_id,
                    "deadline": {
                        $gte: Date.now ()
                    }
                }
            )
            if (queryWithThatAssignment) { //if deadline is not expire 
                //check that is there have to upload any file or not
                let src = sharableFileLink; //file of response
                if (uploadFileBase64) { //if base64 file exist mean that user upload a file
                    const {
                        fileUrl,
                        fileAddStatus
                    }:FileUploadDefaultReturn = await uploadAnyFile (uploadFileBase64, "RES", "pdf")
                    fileAddStatus && (src = fileUrl) //store the uploaded file 
                }
                //first create a new response interface 
        const responseId = idGenerator ("RES") //generate a Response User id
        const responseSlug = slugGenerator (responseId) //generate a slug by using Response id, gender and department
        const createResponseInstance = new ResponseSchema ( //create the instance of response schema
                    {
                        responsesBy: studentProfile?._id,
                        slug: responseSlug,
                        src,
                        submissionDate: Date.now(),
                        responseId,
                        assessment: assessment_id
                    }
                )
                const saveResponse:ResponseModel = await createResponseInstance.save ();
                if (saveResponse) { //if new response created successfully
                    //store the new response id into respective assessment model 
                    const storeIntoAssessment:UpdateReturn  = await Assessment.updateOne ( //update the assessment schema
                        {
                            _id: assessment_id,
                            isDelete: false
                        },
                        {
                            $addToSet: {
                                responses: saveResponse._id
                            }
                        },
                        {multi: true}
                    )
                    if (storeIntoAssessment.modifiedCount != 0) { //if successfully store the new response id into assessment schema
                        res.json ({
                            message: "Thank's for your response we will contact with you soon",
                            status: 201
                        })
                    }else {
                        res.json ({
                            message: "Response has been created but failed ot store in the assessment",
                            status: 406,
    
                        })
                    }
                }else {
                    res.json ({
                        message: "Response failed",
                        status: 406,

                    })
                }
            }else {
                res.json( {
                    message: "Deadline expired please contact with your respective mentor",
                    status: 406,
                })
            }
        }else {
            res.json ({
                message: "Response are already recorded. Thank you",
                status: 200,
            })
        }
    }catch (err) {
        res.json ({
            message: err,
            status: 406
        })
    }
}


//can see own assessment response by assessment slug 
const showMentorAllResponseOfAssessmentBySlug: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const {slug} = req.params //get the slug from params 
        console.log({userId:req.user._id, slug})
        const getAllResponse:AssessmentModelInterface[] = await Assessment.find (
            {
                isDelete: false,
                mentor: req.user._id,
                slug
            }
        ).populate ({
            path: "responses",
            populate: {
                path: "responsesBy",
                select: `
                    fatherName
                    motherName
                    user
                `,
                populate: {
                    path: "user",
                    select: `
                        lastName
                        firstName
                        email
                        sex
                        contact
                        profileImage
                        userId
                        userType
                    `
                }
            }
        }).select (`
            -_id
            responses
        `)
        console.log(getAllResponse)
        if (getAllResponse.length != 0) {
            res.json({
                message: `${getAllResponse.length} response found`,
                status: 202,
                data: getAllResponse
            })
        }else {
            res.json({
                message: "No Response found",
                status: 404,
                data: null
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406,
            data: null
        })
    }
}

//get individual assessment response by slug
const showIndividualResponseBySlug: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const findResponseBySlug:ResponseModel | null = await ResponseSchema.findOne ({
            slug: req.params.slug
        })
        if (findResponseBySlug) { //if response have found
            res.json ({
                message: "Response  found",
                status: 202,
                data: findResponseBySlug
            })
        }else {
            res.json ({
                message: "Response not found",
                status: 404,
                data: null
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406,
            data: null
        })
    }
}

//remark assessment response by slug 
const remarkResponseBySlug: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const {slug} = req.params;
        const {
            status,
            grade
        }:remarkAndGradingBodyInput = req.body
        const updateResponse: UpdateReturn = await ResponseSchema.updateOne (
            {
                slug
            },
            {
                $set: {
                    status,
                    grade
                }
            },
            {multi: true}
        )

        if (updateResponse.modifiedCount != 0) {
            res.json ({
                message: "Response have remarked",
                status: 202
            })
        }else {
            res.json ({
                message: "Response failed to remark",
                status: 406
            })
        }
    }catch (err) {
        res.json ({
            message: err,
            status: 406
        })
    }
}
export {
    createNewResponseHandler,
    showMentorAllResponseOfAssessmentBySlug,
    showIndividualResponseBySlug,
    remarkResponseBySlug
}

// interface CreateResponseReturn {
//     message: string,
//     status: number
// }