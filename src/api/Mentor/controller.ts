import e, {
    Request,
    Response
} from "express"

//models
import User from "../../model/user"
import Mentor from "../../model/mentor"


//utils file 
import slugGenerator from "../../../utils/slugGenerator"
import idGenerator from "../../../utils/idGenerator"
import paginationHandler from "../../../utils/paginationHandler"
import {
    searchQueryHandlerForMentor
} from "../../../utils/controller/Mentor/query"
import {
    uploadAnyFile,
    uploadProfilePictureDefault
} from "../../../utils/fileOrPictureUploadHandler"

//interfaces
import UserInterface from "../../../src/interfaces/User/model"
import MentorModel from "../../../src/interfaces/Mentor/model"
import paginationReturnInterface from "../../interfaces/utils/paginationReturn"
import {
    registrationBodyInput,
    updateMentorInputInterface,
    getAllBodyInterface
} from "../../../src/interfaces/Mentor/bodyInput"
import MentorProfileInterface from "../../../src/interfaces/Mentor/model"
import {
    UpdateReturn
} from "../../interfaces/utils/mongoDB"


//others file 
import bcrypt from "bcrypt"

//register a new mentor 
const registerNewMentor: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const {
            firstName,
            lastName,
            dateOfBirth,
            password,
            retypePassword,
            profileImage: {
                base64:providedBase64,
                size:providedProfileImageSize
            },
            sex,
            salary,
            contactNumber,
            email,
            permanentAddress,
            currentAddress,
        }:registrationBodyInput = req.body; //get all data from body

        //first create a new user interface 
        const mentorUserId = idGenerator ("MNT") //generate a Mentor User id
        const mentorSlug = slugGenerator (mentorUserId, sex) //generate a slug by using Mentor id, gender and department
        let imageUrl:string = "";
        let isUpload:boolean = false
        const hashedPassword= await bcrypt.hash (password, 10)
        //profile picture upload part for Mentor
        if (providedBase64) { //if Mentor provide base64 then it will happen
            const {
                fileAddStatus,
                fileUrl
            } = await uploadAnyFile (providedBase64, mentorUserId) //it will upload profile image
            isUpload = fileAddStatus
            imageUrl = fileUrl
        }else {
            const {
                fileAddStatus,
                fileUrl
            } = await uploadProfilePictureDefault ("mentor", mentorUserId)
            isUpload = fileAddStatus
            imageUrl = fileUrl
        }
        if (isUpload) {
            const createNewUser = new User ({ //create the instance of a Mentor
                slug:mentorSlug,
                userId: mentorUserId,
                userType: "mentor",
                password: hashedPassword,
                firstName,
                lastName,
                email,
                dateOfBirth,
                sex,
                contact: {
                    permanentAddress,
                    currentAddress,
                    mobileNo: contactNumber
                },
                profileImage : imageUrl
            })
            const saveUser:UserInterface = await createNewUser.save() //save the user 
            if (Object.keys(saveUser).length != 0) { //if user create successfully then it will happen
                const newUser:UserInterface = saveUser; 
                const {
                    _id:newUserDatabaseId
                } = newUser
                const createMentorProfile = new Mentor ({ //create a instance of a Mentor
                    salary,
                    user: newUserDatabaseId
                })
                const saveNewMentor: MentorProfileInterface = await createMentorProfile.save ();
                if (Object.keys(saveNewMentor).length != 0) { //if Mentor profile create successfully then
                    const {_id:newMentorDataBaseId}:any = saveNewMentor;
                    const updateUserSchema:UpdateReturn = await User.updateOne (
                        { 
                            _id: newUserDatabaseId,
                            isActive: true,
                            isDeleted: false
                        },
                        {
                            $set : {
                                mentorProfile: newMentorDataBaseId
                            }
                        }
                    )
                    if (updateUserSchema.modifiedCount != 0) {
                        res.json ({
                            message: "Mentor Registration Done!!",
                            status: 201,
                        })
                    }else {
                       res.json ({
                            message: "Mentor Creation failed",
                            status: 406,
                        }) 
                    }

                }else {
                    res.json ({
                        message: "Mentor Creation failed",
                        status: 406,
                    })
                }
            }else {
                res.json ({
                    message: "Mentor Creation failed",
                    status: 406,
                })
            }   
        }else {
            res.json ({
                message: "Profile Upload failed",
                status: 406,
            })
        }

    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}


//delete a mentor by slug 
const deleteMentorBySlugController: (req:Request, res:Response) => void = async (req, res) => {
    try {
        const {slug:mentorSlug} = req.params //get the slug of a user
        const findUser: UserInterface | null = await User.findOne ( //find the user from database
            {
                slug: mentorSlug,
                isDelete: false,
                isActive: true,
                userType: "mentor"
            }
        )
        if (findUser) { //if user found
            const deleteUser:UpdateReturn = await User.updateOne (
                {
                    slug: mentorSlug,
                    isDelete: false,
                    isActive: true ,
                    userType: "mentor"
                },
                {
                    $set: {
                        isDelete: true,
                        isActive: false 
                    }
                },
                {multi: true}
            )
            if (deleteUser.modifiedCount != 0) {
                res.json ({
                    message: "Mentor successfully deleted",
                    status: 202
                })
            }else {
                res.json ({
                    message: "Mentor Delete failed",
                    status: 406
                })
            }
        }else {
            res.json ({
                message: "Mentor Not Found",
                status: 404
            })
        }
        
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
} 

//update mentor details by slug
const updateMentorBySlug :(req:Request, res:Response) => Promise <void>  = async (req, res) => {
    try {
        const {
            common,
            profile
        }:updateMentorInputInterface <MentorModel> = req.body
        const {
            slug:paramsSlug
        } = req.params //get the slug from params
        const findMentor:UserInterface | null = await User.findOne ( //find the mentor
            {
               
                slug: paramsSlug,
                isActive: true,
                isDelete: false,
                userType: "mentor"
            }
        )
        if (findMentor) {  //if mentor find 
            const {
                userType,
                userId,
                _id
            } = findMentor
            let isChange:boolean = false;
            if (userType == "mentor"){ //if user is mentor
                const updateUser:UpdateReturn = await User.updateOne ( //update the main user
                    {   
                        userId,
                        slug: paramsSlug,
                        userType,
                        isDelete: false,
                        isActive: true
                    },
                    {
                        $set: common
                    },
                    {
                        multi: true
                    }
                    
                ) 
                const findProfile: MentorModel | null = await Mentor.findOne ({user: _id})
                let mentorProfile:MentorModel | undefined;
                if (findProfile) { //if mentor profile found
                    mentorProfile = findProfile
                }else {
                    res.json ({
                        message: "Mentor's Profile  Not Found",
                        status: 404
                    })
                }
                const updateProfile: UpdateReturn = await Mentor.updateOne ({ //update the mentor's profile
                    user: _id
                },
                profile,
                {
                    multi: true
                })
                // console.log(updateProfile)
                if (updateProfile.modifiedCount != 0 || updateUser.modifiedCount != 0) {
                    isChange = true
                }
            }
            if (isChange) {
                res.json ({
                    message: "Mentor's Profile Successfully Updated",
                    status: 202
                })
            }else {
                res.json ({
                    message: "Mentor's Profile  Update Failed",
                    status: 406
                })
            }
        }else {
            res.json ({
                message: "Mentor Not found",
                status: 404
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}

//get all mentor controller
const getAllMentorController:(req:Request, res:Response) => Promise <void>  = async (req, res) => {
    try {
        const {
            searchBy,
            dataLimit:inputDataLimit,
            pageNo:inputPageNo
        }: getAllBodyInterface = req.body //get the body data
        const allMentors:UserInterface [] | null = await User.find ({ //get all mentor's
            userType: "mentor",
            isDelete: false,
            isActive: true
        }) 
        const {
            dataLimit: limitData,
            totalPage,
            skipData,
        }:paginationReturnInterface  = paginationHandler (inputDataLimit,allMentors, inputPageNo )

        const query: {
            $and: Object[]
        } = {
            $and: []
        };
        searchQueryHandlerForMentor (query, searchBy) //build the search query
        const findMentors:UserInterface [] | null = await User.find (query).skip (skipData).limit (limitData)
        if (findMentors) { //if mentor found 
           res.json ({
               message: `${findMentors.length} mentor found`,
               mentor: findMentors,
               totalPage,
               dataLimit: inputDataLimit | 5,
               status: 202
           })
        }else {
            res.json ({
                message: "Mentor not found",
                status: 404,
                mentor: null,
                totalPage,
                dataLimit: inputDataLimit | 5
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406,
            mentor: null,
            totalPage: null,
            dataLimit: null
        })
    }
} 

//get individual mentor by slug 
const getIndividualMentorBySlug :(req:Request, res:Response) => Promise <void>  = async (req, res) => {
    try {
        const {slug:inputSlug} = req.params;
        const findMentor: UserInterface | null = await User.findOne ({ //query to find the mentor
            userType: "mentor",
            isDelete: false,
            isActive: true,
            slug: inputSlug
        }).populate ({
            path: "mentorProfile",
            select: `
                -user
                -updatedAt
            `
        })
        if (findMentor) { //if mentor found
            res.json ({
                message: "Mentor found!!",
                status: 202,
                mentor: findMentor
            })
        }else {
            res.json ({
                message: "Mentor not found!!",
                status: 404,
                mentor: null
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406,
            mentor: null
        })
    }
} 
export {
    registerNewMentor,
    deleteMentorBySlugController,
    updateMentorBySlug,
    getAllMentorController,
    getIndividualMentorBySlug
}

