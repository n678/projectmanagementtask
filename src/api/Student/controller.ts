import e, {
    Request,
    Response
} from "express"

//models
import User from "../../model/user"
import Student from "../../model/student"

//interfaces
import UserInterface from "../../interfaces/User/model"
import StudentInterface from "../../../src/interfaces/Student/model"
import {
    UpdateReturn
} from "../../interfaces/utils/mongoDB"
import {
    registrationBodyInput,
    getAllStudentBodyInput
} from  "../../interfaces/Student/bodyInput"
import {
    updateMentorInputInterface
} from "../../interfaces/Mentor/bodyInput"


//utils file 
import slugGenerator from "../../../utils/slugGenerator"
import idGenerator from "../../../utils/idGenerator"
import {
    uploadAnyFile,
    uploadProfilePictureDefault
} from "../../../utils/fileOrPictureUploadHandler"
import {
    searchQueryHandlerForStudent
} from "../../../utils/controller/Student/searchQuery"
import paginationHandler from "../../../utils/paginationHandler"

//others file 
import bcrypt from "bcrypt"


//register a new student 
const registerNewStudentController: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const {
            firstName,
            lastName,
            dateOfBirth,
            password,
            retypePassword,
            motherName,
            fatherName,
            profileImage: {
                base64:providedBase64,
                size:providedProfileImageSize
            },
            sex,
            contactNumber,
            email,
            permanentAddress,
            currentAddress
        }:registrationBodyInput = req.body; //get all data from body
        //first create a new user interface 
        const studentId = idGenerator ("STD") //generate a student User id
        const studentSlug = slugGenerator (studentId, sex) //generate a slug by using student id, gender and department
        let imageUrl:string = "";
        let isUpload:boolean = false
        const hashedPassword= await bcrypt.hash (password, 10)
        //profile picture upload part for student
        if (providedBase64) { //if student provide base64 then it will happen
            const {
                fileAddStatus,
                fileUrl
            } = await uploadAnyFile (providedBase64, studentId) //it will upload profile image
            isUpload = fileAddStatus
            imageUrl = fileUrl
        }else {
            const {
                fileAddStatus,
                fileUrl
            } = await uploadProfilePictureDefault ("student", studentId)
            isUpload = fileAddStatus
            imageUrl = fileUrl
        }
        if (isUpload) {
            const createNewUser = new User ({ //create the instance of a student
                slug:studentSlug,
                userId: studentId,
                userType: "student",
                password: hashedPassword,
                firstName,
                lastName,
                email,
                dateOfBirth,
                sex,
                contact: {
                    permanentAddress,
                    currentAddress,
                    mobileNo: contactNumber
                },
                profileImage : imageUrl
            })
            const saveUser: UserInterface = await createNewUser.save() //save the user 
            if (Object.keys(saveUser).length != 0) { //if user create successfully then it will happen
                const newUser:UserInterface = saveUser; 
                const {
                    _id:newUserDatabaseId
                } = newUser
                const createStudentProfile = new Student ({ //create a instance of a student
                    fatherName,
                    motherName,
                    user: newUserDatabaseId
                })
                const saveNewStudent: StudentInterface = await createStudentProfile.save ();
                if (Object.keys(saveNewStudent).length != 0) { //if student profile create successfully then
                    const {_id:newTeacherDataBaseId}:any = saveNewStudent;
                    const updateUserSchema:UpdateReturn = await User.updateOne (
                        { 
                            _id: newUserDatabaseId,
                            isActive: true,
                            isDeleted: false
                        },
                        {
                            $set : {
                                studentProfile: newTeacherDataBaseId
                            }
                        }
                    )
                    if (updateUserSchema.modifiedCount != 0) {
                        res.json ({
                            message: "Student Registration Done!!",
                            status: 201,
                        })
                    }else {
                       res.json ({
                            message: "Student Creation failed",
                            status: 406,
                        }) 
                    }

                }else {
                    res.json ({
                        message: "Student Creation failed",
                        status: 406,
                    })
                }
            }else {
                res.json ({
                    message: "Student Creation failed",
                    status: 406,
                })
            }   
        }else {
            res.json ({
                message: "Profile Upload failed",
                status: 406,
            })
        }

    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}

//delete a student by slug 
const deleteStudentBySlug: (req: Request, res: Response) => Promise<void> = async(req, res) => {
    try {
        const {slug:studentSlug} = req.params //get the slug of a user
        const findUser: UserInterface | null = await User.findOne ( //find the user from database
            {
                slug: studentSlug,
                isDelete: false,
                isActive: true,
                userType: "student"
            }
        )
        if (findUser) { //if user found
            const deleteUser:UpdateReturn = await User.updateOne (
                {
                    slug: studentSlug,
                    isDelete: false,
                    isActive: true ,
                    userType: "student"
                },
                {
                    $set: {
                        isDelete: true,
                        isActive: false 
                    }
                },
                {multi: true}
            )
            if (deleteUser.modifiedCount != 0) {
                res.json ({
                    message: "Student successfully deleted",
                    status: 202
                })
            }else {
                res.json ({
                    message: "Student Delete failed",
                    status: 406
                })
            }
        }else {
            res.json ({
                message: "Student Not Found",
                status: 404
            })
        }
    }catch (err) {
        console.log(err);
        res.json ({
            message: err,
            status: 406
        })
    }
}

//update student by slug 
const updateStudentBySlug :(req:Request, res:Response) => Promise <void>  = async (req, res) => {
    try {
        const {
            common,
            profile
        }:updateMentorInputInterface <StudentInterface> = req.body
        const {
            slug:paramsSlug
        } = req.params //get the slug from params
        const findStudent:UserInterface | null = await User.findOne ( //find the student
            {
               
                slug: paramsSlug,
                isActive: true,
                isDelete: false,
                userType: "student"
            }
        )
        if (findStudent) {  //if student find 
            const {
                userType,
                userId,
                _id
            } = findStudent
            let isChange:boolean = false;
            if (userType == "student"){ //if user is student
                const updateUser:UpdateReturn = await User.updateOne ( //update the main user
                    {   
                        userId,
                        slug: paramsSlug,
                        userType,
                        isDelete: false,
                        isActive: true
                    },
                    {
                        $set: common
                    },
                    {
                        multi: true
                    }
                    
                ) 
                const findProfile: StudentInterface | null = await Student.findOne ({user: _id})
                // let studentProfile:StudentInterface | undefined;
                if (!findProfile) { //if student profile not found
                     res.json ({
                        message: "Mentor's Profile  Not Found",
                        status: 404
                    })
                }
                const updateProfile: UpdateReturn = await Student.updateOne ({ //update the student's profile
                    user: _id
                },
                profile,
                {
                    multi: true
                })
                if (updateProfile.modifiedCount != 0 || updateUser.modifiedCount != 0) {
                    isChange = true
                }
            }
            if (isChange) {
                res.json ({
                    message: "Student's Profile Successfully Updated",
                    status: 202
                })
            }else {
                res.json ({
                    message: "Student's Profile  Update Failed",
                    status: 406
                })
            }
        }else {
            res.json ({
                message: "Student Not found",
                status: 404
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}

//can see all student profiles
const showAllStudentHandler:(req:Request, res:Response) => Promise <void>  = async (req, res) => {
    try {
        const {
            searchBy,
            dataLimit,
            pageNo
        }:getAllStudentBodyInput = req.body
        const query = {
            $and: [
                {
                    isActive: true,
                    isDelete: false,
                    "userType": "student"
                }
            ]
        }
        searchQueryHandlerForStudent (query, searchBy);
        const getALlStudent:StudentInterface [] = await Student.find ({isDelete: false, isActive: true})
        const {
            dataLimit: limit,
            totalPage,
            skipData
        } = paginationHandler (dataLimit,getALlStudent, pageNo )
        const showAllStudent:UserInterface [] = await User.find (query).populate ({
            path: "studentProfile"
        }).select (`-password`).limit(limit).skip (skipData)
        if (showAllStudent.length != 0) {
            res.json ({
                message: `${showAllStudent.length} student found`,
                status: 404,
                data: showAllStudent,
                totalPage: totalPage
            })
        } else {
            res.json ({
                message: "Student not found",
                status: 404,
                data: null,
                totalPage: null
            })
        }

    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406,
            data: null,
            totalPage: null
        })
    }
}

//show individual student profile by slug 
const showIndividualStudentBySlug:(req:Request, res:Response) => Promise <void>  = async (req, res) => {
    try {
        const {slug} = req.params;
        const findUser: UserInterface | null = await User.findOne (
            {
                isActive: true,
                isDelete: false,
                slug,
                userType: "student"
            }
        ).select (`
            -password
            -otp
        `).populate ({
            path: "studentProfile",
            select: `
                fatherName
                motherName
            `
        })
        if (findUser) {
            res.json ({
                message: "Student found!!",
                status: 202,
                data: findUser
            })
        }else {
            res.json ({
                message: "Student not found!!",
                status: 404,
                data: null
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            data: null,
            status: 406
        })
    }
}

export {
    registerNewStudentController,
    deleteStudentBySlug,
    updateStudentBySlug,
    showAllStudentHandler,
    showIndividualStudentBySlug
}