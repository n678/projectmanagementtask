import mongoose , {Schema} from "mongoose";
import MentorModelInterface from "../interfaces/Admin/model"


const mentorSchema = new Schema <MentorModelInterface>({
    salary: {
        type: String,
        required: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
},{
    timestamps: true
})

export default mongoose.model <MentorModelInterface> ("Mentor", mentorSchema)