import mongoose , {Schema} from "mongoose";
import StudentModelInterface from "../interfaces/Student/model"


const studentSchema = new Schema <StudentModelInterface>({
    fatherName: {
        type: String,
        required: true
    },
    motherName: {
        type: String,
        required: true
    },
    assessments: [
        {
            type: Schema.Types.ObjectId,
            ref: "Assessment"
        }
    ],
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
},{
    timestamps: true
})

export default mongoose.model <StudentModelInterface> ("Student", studentSchema)