import UserModelInterface from "../../src/interfaces/User/model"

declare global{
    namespace Express {
        interface Request {
            user: UserModelInterface //add a new user interface in the request interface,
            isAuth: boolean
        }
    }
}
