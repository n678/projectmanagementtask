const convertToRegex: (input:string) => Object = (input) => {
    const str = `${input}`
    const regex = new RegExp (str, 'gi')
    return regex;
}

export default convertToRegex