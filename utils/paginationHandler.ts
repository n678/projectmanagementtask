import paginationReturnInterface from "../src/interfaces/utils/paginationReturn"
const paginationHandler: (inputDataLimit: number, dataCollection: any, inputPageNo:number) => paginationReturnInterface = (inputDataLimit, dataCollection, inputPageNo) => {
    const limitData:number = inputDataLimit || 5; //if data limit has given from body then that will be apply otherwise global default data limit will b apply
    const totalData:number = dataCollection.length //get all data count 
    const pageNo:number = inputPageNo ? inputPageNo : 1 //if page number has given from body then that will be apply otherwise global default page number will b apply
    const skipData:number = (pageNo * limitData) - limitData //this amount of data will be skip 
    const totalPage:number = Math.ceil (totalData / limitData) //total this amount of page we need
    return {
        dataLimit: limitData,
        skipData,
        totalPage
    }
}

export default paginationHandler