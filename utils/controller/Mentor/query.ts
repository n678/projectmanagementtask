import convertToRegex from "../../../utils/makeRegex"

type inputAndOutput = {
    $and: Object[]
}
const searchQueryHandlerForMentor =  (struct:inputAndOutput, searchBy: string): void => {
    //convert search by into regex first 
    const searchRegex:Object =  convertToRegex (searchBy)
    //do the searching part 
    struct.$and.push (
        {
            $or : [
                { 
                    "userId": searchRegex //search by user id 
                },
                {
                    "firstName": searchRegex //search by first name
                },
                {
                    "lastName": searchRegex //search by last name
                }
            ]
        }
    ) 
}

export {
    searchQueryHandlerForMentor
}